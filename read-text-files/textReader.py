#!/usr/bin/env python
# -*- coding: utf-8 -*-

def GetFileLines( path ):
    lines = []

    with open( path, "r" ) as fileHandle:
        lines = fileHandle.readlines()

    return lines
    
